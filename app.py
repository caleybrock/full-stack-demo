from flask import Flask, render_template
app = Flask(__name__)

hotels = [{
  'id' : 12,
  'hotel_name' : 'Center Hilton',
  'num_reviews' : 209,
  'address' : '12 Wall Street, Very Large City',
  'num_stars' : 4,
  'amenities' : ['Wi-Fi', 'Parking'],
  'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
  'price' : 132.11
  },
  {
    'id' : 13,
    'hotel_name' : 'Center Caley',
    'num_reviews' : 777,
    'address' : '12 Caley Street, Very Large City',
    'num_stars' : 5,
    'amenities' : ['Wi-Fi', 'Parking'],
    'image_url' : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
    'price' : 170.11
  }
]

@app.route('/demo')
def demo():
  data = [{
    'name': '1'
  }, {
    'name': '2'
  }]
  return render_template('demo.html', name='world', data=data)

@app.route('/')
@app.route('/form')
def form():
  return render_template('form.html')

@app.route('/table')
def table():
  return render_template('table.html', hotels=hotels)

if __name__ == "__main__":
  app.run(debug=True)